package org.astegiano.kotlin.io.reactivex

import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription

abstract class BaseSubscriber<SubscribedTo> : Subscriber<SubscribedTo> {

    private lateinit var subscription: Subscription

    override fun onSubscribe(subscription: Subscription?) {
        if (subscription != null) {
            this.subscription = subscription
        }
    }

    override fun onNext(instance: SubscribedTo) {
    }

    override fun onComplete() {
    }

    override fun onError(t: Throwable?) {
    }
}
