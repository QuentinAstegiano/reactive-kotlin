package org.astegiano.kotlin.io.reactivex


interface Observer<in TypeToObserve> {

    /**
     * Provides the Observer with the means of cancelling (disposing) the
     * connection (channel) with the Observable in both
     * synchronous (from within [.onNext]) and asynchronous manner.
     * @param d the Disposable instance whose [Disposable.dispose] can
     * * be called anytime to cancel the connection
     * *
     * @since 2.0
     */
    fun onSubscribe(d: Disposable)

    /**
     * Provides the Observer with a new item to observe.
     *
     * The [Observable] may call this method 0 or more times.
     *
     * The `Observable` will not call this method again after it calls either [.onComplete] or
     * [.onError].
     * @param typeToObserve the item emitted by the Observable
     */
    fun onNext(typeToObserve: TypeToObserve)

    /**
     * Notifies the Observer that the [Observable] has experienced an error condition.
     *
     * If the [Observable] calls this method, it will not thereafter call [.onNext] or
     * [.onComplete].
     * @param exception the exception encountered by the Observable
     */
    fun onError(exception: Throwable)

    /**
     * Notifies the Observer that the [Observable] has finished sending push-based notifications.
     *
     * The [Observable] will not call this method if it calls [.onError].
     */
    fun onComplete()

}