# What is it ?

You are probably looking for RxKotlin : https://github.com/ReactiveX/RxKotlin 

My aim is to do a pure Kotlin implementation of ReactiveX, using Kotlin 1.1 coroutines... and I'm just getting started, and I'm learning Kotlin & Kotlin coroutines along the way :)
