package org.astegiano.kotlin.io.reactivex

import kotlinx.coroutines.experimental.CommonPool
import kotlinx.coroutines.experimental.launch
import org.reactivestreams.Publisher
import org.reactivestreams.Subscriber

class Flowable<PublishedType>(private val producer: Producer<PublishedType>) : Publisher<PublishedType> {

    fun subscribe(onSuccess: (PublishedType) -> Unit,
                  onFailure: (Throwable) -> Unit = {}) {
        subscribe(object : BaseSubscriber<PublishedType>() {
            override fun onNext(t: PublishedType) {
                launch(CommonPool) {
                    onSuccess.invoke(t)
                }
            }

            override fun onError(t: Throwable?) {
                if (t != null) {
                    launch(CommonPool) {
                        onFailure(t)
                    }
                }
            }
        })
    }

    override fun subscribe(subscriber: Subscriber<in PublishedType>?) {
        if (subscriber != null) {
            while (producer.hasNext()) {
                try {
                    subscriber.onNext(producer.next())
                } catch (t : Throwable) {
                    subscriber.onError(t)
                }
            }
        }
    }
}

interface Producer<out Type> {
    fun next() : Type
    fun hasNext() : Boolean
}

fun <PublishedType> just(item: PublishedType) : Flowable<PublishedType> {
    return Flowable(object : Producer<PublishedType> {
        private var sent = false
        override fun next(): PublishedType {
            sent = true
            return item
        }

        override fun hasNext(): Boolean {
            return !sent
        }
    })
}

fun <PublishedType> fromCallable(call: () -> PublishedType) : Flowable<PublishedType> {
    return Flowable(object : Producer<PublishedType> {
        private var sent = false
        override fun next(): PublishedType {
            sent = true
            return call()
        }

        override fun hasNext(): Boolean {
            return !sent
        }
    })
}
