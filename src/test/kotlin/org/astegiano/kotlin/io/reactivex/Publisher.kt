package org.astegiano.kotlin.io.reactivex

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test

class Tests {

    @Test
    fun `flowable should send a unique value to subscriber`() {
        // Given
        val list = mutableListOf<String>()

        // When
        just("simple").subscribe(onSuccess = { list.add(it) })
        Thread.sleep(100)

        // Then
        assertThat(list).hasSize(1)
        assertThat(list[0]).isEqualTo("simple")
    }

    @Test
    fun `flowable should send a callable value to subscriber`() {
        // Given
        val list = mutableListOf<String>()

        // When
        fromCallable {
            "callable"
        }.subscribe(onSuccess = { list.add(it) })
        list.add("main")
        Thread.sleep(100)

        // Then
        assertThat(list).hasSize(2)
        assertThat(list[0]).isEqualTo("main")
        assertThat(list[1]).isEqualTo("callable")
    }

    @Test
    fun `flowable should send error message to the failure listener`() {
        // Given
        val list = mutableListOf<String>()
        val errors = mutableListOf<Throwable>()

        // When
        fromCallable {
            throw UnsupportedOperationException()
        }.subscribe(
                onSuccess = { list.add(it) },
                onFailure = { errors.add(it) })
        Thread.sleep(100)

        // Then
        assertThat(list).isEmpty()
        assertThat(errors).hasSize(1)
    }

}
